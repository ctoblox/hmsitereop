import {Component, OnInit} from '@angular/core';
import {FormBaseComponent} from '@app/shared/components/form-base/form-base.component';
import {ContactUsService} from '@app/contact-us/contact-us.service';
import {FormBuilder, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.less']
})
export class ContactUsComponent extends FormBaseComponent implements OnInit {
  private successStr: string;
  private failedStr: string;
  readonly subjects = ['getOffer', 'callMe', 'proQuestion', 'report', 'openUser'];

  constructor(private service: ContactUsService,
              location: Location,
              private   translate: TranslateService,
              private formBuilder: FormBuilder) {
    super(location);
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      fullName: [null, Validators.required],
      company: [null],
      email: [null, [Validators.required, Validators.email]],
      mobile: [null],
      subject: [''],
      body: [null]
    });

    this.translate.get('contactUs.success').subscribe(str => {
      this.successStr = str;
    });
    this.translate.get('contactUs.failed').subscribe(str => {
      this.failedStr = str;
    });
  }

  postMe(model) {


    this.service.create(model).subscribe(res => {
      if (!res) {
        return this.successMsg = this.successStr;
      }
      this.disableBtn = false;
      this.failed = this.failedStr;

    });
  }
}
