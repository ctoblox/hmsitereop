import { Injectable } from '@angular/core';
import {BaseService} from '@app/shared/base.service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ContactUsService extends BaseService {

  constructor(http: HttpClient) {
    super('contactUs', http);
  }
}
