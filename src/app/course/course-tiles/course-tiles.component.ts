import {Component, OnInit} from '@angular/core';
import {CourseService} from '@app/course/course.service';
import {Course} from '@app/course/course.model';

@Component({
  selector: 'app-course-tiles',
  templateUrl: './course-tiles.component.html',
  styleUrls: ['./course-tiles.component.less']
})
export class CourseTilesComponent implements OnInit {

  courses: Course[];

  constructor(private service: CourseService) {
  }


  ngOnInit() {
    this.service.getList().subscribe(x => {
      this.courses = x.slice(0, 4);
    });
  }

}
