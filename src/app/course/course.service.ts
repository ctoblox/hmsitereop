import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from '@app/shared/base.service';

import { share } from 'rxjs/operators';
import {Courses} from '@app/course/course.model';
import {Observable, of} from 'rxjs/index';

let COURSES: Courses;

@Injectable()
export class CourseService extends BaseService {


  constructor(http: HttpClient) {
    super('course', http);
  }

  getList(): Observable<any[]> {

    /** try get from cache*/
    if (COURSES && COURSES.length) {
      return of(COURSES);
    }

    /** get from server*/
    const res = super.getList().pipe(share());

    /** add to cache*/
    res.subscribe(data => {
      COURSES = data;
    });

    return res;
  }

}
