import {Component, OnInit} from '@angular/core';
import {CourseService} from '@app/course/course.service';
import {ActivatedRoute} from '@angular/router';
import {Courses} from '@app/course/course.model';
import {Location} from '@angular/common';
import {FormBuilder, Validators} from '@angular/forms';
import {FormBaseComponent} from '@app/shared/components/form-base/form-base.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-course-order',
  templateUrl: './course-order.component.html',
  styleUrls: ['./course-order.component.less']
})
export class CourseOrderComponent extends FormBaseComponent implements OnInit {
  courses: Courses;
  private successStr: string;
  private failedStr: string;

  constructor(private service: CourseService,
              private formBuilder: FormBuilder,
              private   translate: TranslateService,
              location: Location,
              private  route: ActivatedRoute) {
    super(location);
  }


  ngOnInit() {
    this.service.getList().subscribe(res => {
      this.courses = res;
    });

    this.form = this.formBuilder.group({
      courseId: [this.route.snapshot.params['id'], Validators.required],
      fullName: [null, Validators.required],
      company: [null],
      spots: [1],
      email: [null, [Validators.required, Validators.email]],
      mobile: [null, Validators.required],
      comments: [null]
    });

    this.translate.get('order.success').subscribe(str => {
      this.successStr = str;
    });
    this.translate.get('order.failed').subscribe(str => {
      this.failedStr = str;
    });
  }

  postMe() {
    this.service.create(this.form.value).subscribe(res => {
      if (!res) {
        return this.successMsg = this.successStr;
      }
      this.disableBtn = false;
      this.failed = this.failedStr;
    });
  }

}
