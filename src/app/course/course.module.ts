import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoursesListComponent} from './courses-list/courses-list.component';
import {CourseDetailsComponent} from './course-details/course-details.component';
import {CourseService} from './course.service';
import {SharedModule} from '@app/shared/shared.module';
import {CourseResolve} from '@app/course/course.resovle';
import {McBreadcrumbsModule} from 'ngx-breadcrumbs';
import {AppRoutingModule} from '@app/app-routing.module';
import { CourseTilesComponent } from './course-tiles/course-tiles.component';
import { CourseOrderComponent } from './course-order/course-order.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    McBreadcrumbsModule.forRoot()
  ],
  declarations: [CoursesListComponent, CourseDetailsComponent, CourseOrderComponent],
  providers: [CourseService, CourseResolve]
})
export class CourseModule {

}
