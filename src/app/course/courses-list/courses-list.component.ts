import { Component, OnInit } from '@angular/core';
import { CourseService} from '../course.service';
import {Course} from '@app/course/course.model';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.less']
})
export class CoursesListComponent implements OnInit {
  courses: Course[];
  constructor(private service: CourseService) { }


  ngOnInit() {
    this.service.getList().subscribe(x => {
      this.courses = x;
    });
  }
}
