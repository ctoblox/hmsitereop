import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {CourseService} from '@app/course/course.service';


@Injectable()
export class CourseResolve implements Resolve<any> {

  constructor(private coursesService: CourseService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.coursesService.getSingle(route.params['id']);
  }
}
