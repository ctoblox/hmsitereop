import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Course} from '@app/course/course.model';


@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.less']
})
export class CourseDetailsComponent implements OnInit {

  course: Course;
  paragraphs = ['shortDesc', 'audience', 'subjects', 'testInfo', 'certificates', 'comments'];


  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.course = this.route.snapshot.data['course'];
    });
  }


}
