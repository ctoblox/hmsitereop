export interface Course {
  id: string;
  courseId?: number;
  title: string;
  link?: string;
  shortDesc: string;
  img: string;
  courseDate: string;
  endDate?: string;
  audience: string;
  objectives: string;
  timeLength: string;
  serial?: string;
  price: string;
  location?: string;
  teacher?: string;
  subjects: string;
}

export declare type Courses = Course[];

export class CourseStudent {
  courseId: number;
  fullName: string;
  email?: string;
  mobile?: string;
  company: string;
  spots: number;
  comments: string;

  constructor(id) {
    this.courseId = id;
    this.spots = 1;
  }
}
