import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OurServicesDetailsComponent} from './our-services-details/our-services-details.component';
import {OurServicesService} from './our-services.service';
import {OurServicesListComponent} from './our-services-list/our-services-list.component';
import {McBreadcrumbsModule} from 'ngx-breadcrumbs';
import {AppRoutingModule} from '@app/app-routing.module';
import {SharedModule} from '@app/shared/shared.module';
import {OurServiceResolve} from '@app/our-services/our-services.resolve';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    McBreadcrumbsModule.forRoot()
  ],
  declarations: [OurServicesDetailsComponent, OurServicesListComponent],
  providers: [OurServicesService, OurServiceResolve]
})
export class OurServicesModule {
}
