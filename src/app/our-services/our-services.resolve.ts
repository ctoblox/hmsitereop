
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {OurServicesService} from '@app/our-services/our-services.service';
import {Injectable} from '@angular/core';

@Injectable()
export class OurServiceResolve implements Resolve<any> {

  constructor(private service: OurServicesService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.service.getSingle(route.params['id']);
  }
}
