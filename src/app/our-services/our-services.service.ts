import {Injectable} from '@angular/core';

import {StaticDataService} from '@app/shared/static-data';
import {NavLink} from '@app/shared/components/header/NavLink';
import {TranslateService} from '@ngx-translate/core';
import {OurService} from '@app/our-services/our-services.model';


const OUR_SERVICES: OurService[] = [
  {
    id: 'vb',
    titleClass: 'blueback',
    path: 'vibration-monitoring',
    piclessTile: true
  },
  {

    id: 'shivur',
    path: 'laser-alignment',
    titleClass: 'blueback',
    piclessTile: true
  },
  {
    id: 'refubrish',
    path: 'installation-and-refurbishing',
    titleClass: 'page double ',
    overlayClass: 'blue',
    piclessTile: false
  },
  {
    id: 'lipuf',
    path: 'motors-rewinding',
    titleClass: 'page',
    piclessTile: false
  },
  {
    id: 'align',
    path: 'dynamic-balancing',
    titleClass: 'blueback',
    piclessTile: true
  }
];

@Injectable()
export class OurServicesService extends StaticDataService {

  constructor() {
    super();
  }

  get(): OurService[] {
    // return this.buildLink('services', OUR_SERVICES);
    return OUR_SERVICES;
  }


  getSingle(path: string) {
    return this.get().find(item => item.path === path);
  }

  getLinks(): NavLink[] {
    return this.get().map(x => {
      return {id: x.id, path: x.path};
    });
  }
}


