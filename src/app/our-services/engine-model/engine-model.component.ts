import { WINDOW } from '@ng-toolkit/universal';
import {Component, HostListener, OnInit, Inject} from '@angular/core';

@Component({
  selector: 'app-engine-model',
  templateUrl: './engine-model.component.html',
  styleUrls: ['./engine-model.component.less']
})
export class EngineModelComponent implements OnInit {
  exploded = false;
//  scrolled = false;
  path = '/assets/images/model/';
  parts = ['fanCover_bottom', 'fan', 'body_bottom', 'backCover', 'rotor_bottom', 'body_top', 'fanCover_top', 'frontHood_bottom', 'um_bottom', 'rotor_top', 'frontHood_top', 'um_top'];

  // parts = ['body_bottom',  'rotor_top', 'body_top'];
  constructor(@Inject(WINDOW) private window: Window,) {
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.exploded = this.isElementInViewport('body_top');

  }

  isElementInViewport(id) {
    const rect = document.getElementById(id).getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
    );
  }

  ngOnInit() {
  }

}
