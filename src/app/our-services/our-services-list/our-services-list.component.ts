import { Component, OnInit } from '@angular/core';
import {OurServicesService} from '@app/our-services/our-services.service';
import {OurService} from '@app/our-services/our-services.model';

@Component({
  selector: 'app-our-services-list',
  templateUrl: './our-services-list.component.html',
  styleUrls: ['./our-services-list.component.less']
})
export class OurServicesListComponent implements OnInit {
  ourServices: OurService[];
  constructor(private  service: OurServicesService) { }

  ngOnInit() {
    this.ourServices = this.service.get();
  }

}
