export interface OurService {
  title?: string;
  path: string;
  id: string;
  icon?: string;
  img?: string;
  scndImage?: string;
  link?: string;
  desc?: string;
  titleClass?: string;
  overlayClass?: string;
  text?: string;
  piclessTile?: boolean;
}
