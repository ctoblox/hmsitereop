import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OurService} from '@app/our-services/our-services.model';

@Component({
  selector: 'app-our-services-details',
  templateUrl: './our-services-details.component.html',
  styleUrls: ['./our-services-details.component.less']
})
export class OurServicesDetailsComponent implements OnInit {

  service: OurService;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.service = this.route.snapshot.data['service'];
    });

  }

}
