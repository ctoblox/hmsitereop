import {Component, OnInit} from '@angular/core';
import { OurServicesService} from '../our-services.service';
import {OurService} from '@app/our-services/our-services.model';

@Component({
  selector: 'app-our-services-tiles',
  templateUrl: './our-services-tiles.component.html',
  styleUrls: ['./our-services-tiles.component.less', '../../home/home.component.less']
})
export class OurServicesTilesComponent implements OnInit {
  ourServices: OurService[];

  constructor(private ourServicesService: OurServicesService) {
  }

  ngOnInit() {
    this.ourServices = this.ourServicesService.get();
  }

}

