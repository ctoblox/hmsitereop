import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';


import { HomeComponent } from './home.component';
import {LazyLoadImageModule} from 'ng-lazyload-image';
import {FadeOnScrollDirective} from '../fade-on-scroll.directive';
import { ProjectGalleryComponent } from './project-gallery/project-gallery.component';
import {OurServicesTilesComponent} from '../our-services/our-services-tiles/our-services-tiles.component';
import {OurServicesService} from '../our-services/our-services.service';
import {ProductTilesComponent} from '../product/product-tiles/product-tiles.component';
import {AppRoutingModule} from '@app/app-routing.module';
import {SharedModule} from '@app/shared/shared.module';
import {CustomersComponent} from '@app/customers/customers.component';
import {CourseTilesComponent} from '@app/course/course-tiles/course-tiles.component';
import {AboutTilesComponent} from '@app/about/about-tiles/about-tiles.component';
import { IntroComponent } from './intro/intro.component';
import {ContactTilesComponent} from '@app/contact-us/contact-tiles/contact-tiles.component';
import {EngineModelComponent} from '@app/our-services/engine-model/engine-model.component';


@NgModule({
  imports: [
    CommonModule,
    LazyLoadImageModule,
    SharedModule,
    AppRoutingModule
  ],
  declarations: [
    HomeComponent,
    FadeOnScrollDirective,
    ProjectGalleryComponent,
    OurServicesTilesComponent,
    ProductTilesComponent,
    EngineModelComponent,
    CourseTilesComponent,
    AboutTilesComponent,
    ContactTilesComponent,
    CustomersComponent,
    IntroComponent
  ],
  providers:    [ OurServicesService ]
})
export class HomeModule {

}
