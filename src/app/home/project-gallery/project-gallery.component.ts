import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-project-gallery',
  templateUrl: './project-gallery.component.html',
  styleUrls: ['./project-gallery.component.less']
})
export class ProjectGalleryComponent implements OnInit {
  hasMore = true;
  loading = false;

  projects: Project[] = [
    {id: '10kHp'},
    {id: '2990Rpm'},
    {id: 'maxima'},
    {id: 'stator'},
    {id: 'shivur'},
    {id: 'vac'}
  ];
  clickId: String;


  constructor() {
  }

  ngOnInit() {
  }

  getMore() {
    this.loading = true;
    setTimeout(() => {
      this.hasMore = false;
    }, 1 * 500);

  }
}

export interface Project {
  id: string;
  // title: string;
  // file: string;
  // desc: string;
}
