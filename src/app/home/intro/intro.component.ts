import {Component, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.less']
})
export class IntroComponent implements OnInit {

  showMovie = false;
  playIt;

  constructor(private sanitizer: DomSanitizer) {
    this.playIt = this.sanitize();
  }

  ngOnInit() {
  }

  playMovie() {
    this.showMovie = true;
    this.playIt = this.sanitize(1);
  }

  closeMovie() {
    this.showMovie = false;
    this.playIt = this.sanitize();
  }

  private sanitize(play: number = 0) {
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/6Mlb85ez_oM?rel=0&showinfo=0&autoplay=' + play);
  }
}
