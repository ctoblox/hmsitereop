import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {OurServicesDetailsComponent} from './our-services/our-services-details/our-services-details.component';
import {AboutComponent} from './about/about.component';
import {ProductDetailsComponent} from './product/product-details/product-details.component';
import {CoursesListComponent} from './course/courses-list/courses-list.component';
import {CourseDetailsComponent} from './course/course-details/course-details.component';
import {CourseResolve} from '@app/course/course.resovle';

import {ContactUsComponent} from '@app/contact-us/contact-us.component';
import {CourseOrderComponent} from '@app/course/course-order/course-order.component';
import {ProductResolve} from '@app/product/product.resolve';
import {OurServiceResolve} from '@app/our-services/our-services.resolve';


const routes: Routes = [

  {path: 'home', redirectTo: '/', pathMatch: 'full', data: {breadcrumbs: 'home'}},
  {path: 'contact-us', component: ContactUsComponent, data: { breadcrumbs: 'contactUs',  menuVisible: true}},
  {path: 'about', component: AboutComponent, data: {breadcrumbs: 'about', menuVisible: true}},
  // {
  //   path: ':page',
  //   component:PageComponent,
  //   data: {
  //     breadcrumbs: '{{service.title}}'
  //   },
  //   resolve: {
  //     service: OurServiceResolve
  //   },
  //   children: [
  //
  //   ]
  // },
  {
    path: 'services', data: {breadcrumbs: 'ourServices'},
    children: [
      {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
        // component: OurServicesListComponent,
        // data: {
        //   menuVisible: true
        // }
      },
      {
        path: ':id',
        component: OurServicesDetailsComponent,
        data: {
          breadcrumbs: '{{service.id}}',
          menuHalfKey: true
        },
        resolve: {
          service: OurServiceResolve
        }
      }

    ]
  },
  {
    path: 'products', data: {breadcrumbs: 'products'},
    children: [
      {
        path: '',
        redirectTo: '/',
        pathMatch: 'full'
        // component: ProductListComponent,
        // data: {
        //   menuVisible: true
        // }
      },
      {
        path: ':id',
        component: ProductDetailsComponent,
        data: {
          breadcrumbs: '{{product.id }}',
          menuHalfKey: true
        },
        resolve: {
          product: ProductResolve
        }
      }
    ]
  },
  {
    path: 'courses', data: {breadcrumbs: 'courses'},
    children: [
      {
        path: '',
        component: CoursesListComponent,
        data: {
          menuVisible: true
        }
      },
      {
        path: 'order',
        data: {
          breadcrumbs: 'order',
          menuVisible: true
        },
        children: [
          {
            path: '',
            component: CourseOrderComponent
          },
          {
            path: ':id',
            component: CourseOrderComponent
          }
        ]
      },
      {
        path: ':id',
        component: CourseDetailsComponent,
        data: {
          breadcrumbs: '{{course.title}}',
          menuHalfKey: true
        },
        resolve: {
          course: CourseResolve
        }
      }
    ]
  },
  { path: '', component: HomeComponent, data: {breadcrumbs: 'home'}},
  { path: 'service/55-שיוור-בלייזר', redirectTo: 'services/laser-alignment' },
  { path: 'service/56-איזונים-דינאמיים', redirectTo: 'services/dynamic-balancing' },
  { path: 'service/57-עבודות-ליפוף', redirectTo: 'services/motors-rewinding' },
  { path: 'service/58-התקנה-ושיפוץ-ציוד-סובב', redirectTo: 'services/installation-and-refurbishing' },
  { path: 'service/59-ניטור-רעידות', redirectTo: 'services/vibration-monitoring' },
  { path: 'product/547-טבעות-הארקה', redirectTo: 'products/grounding-rings' },
  { path: 'product/561-חולצים-הידראוליים', redirectTo: 'products/hydraulic-pullers' },
  { path: 'product/563-מכונות-איזון', redirectTo: 'products' },
  { path: 'product/562-שימסים-עשויים-פלדת-אל-חלד', redirectTo: 'products/shims' },
  { path: 'product/542-מכשירי-שיוור-בלייזר', redirectTo: 'products/laser-alignment-instruments' },
  { path: 'product/544-מכשור-אלקטרוני-לבדיקת-מנועי-חשמל', redirectTo: 'products/electronic-testing-systems' },
  { path: 't/1249-אודות', redirectTo: 'about' },
  { path: 'צור-קשר', redirectTo: 'contact-us' },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
