import {WindowModule} from '@ng-toolkit/universal';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';


import {HomeModule} from './home/home.module';
import {OurServicesModule} from './our-services/our-services.module';
import {AboutComponent} from './about/about.component';
import {ProductModule} from './product/product.module';
import {CourseModule} from './course/course.module';
import {McBreadcrumbsConfig, McBreadcrumbsModule} from 'ngx-breadcrumbs';
// import {WINDOW_PROVIDERS} from '@app/window.service';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {SharedModule} from '@app/shared/shared.module';
import {ContactUsService} from '@app/contact-us/contact-us.service';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgxJsonLdModule} from 'ngx-json-ld';
import {CommonModule} from '@angular/common';

// import {registerLocaleData, CommonModule} from '@angular/common';
// import localeHe from '@angular/common/locales/he';
//
// registerLocaleData(localeHe, 'he');
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json?v0207');
}

@NgModule({
  imports: [
    CommonModule,
    WindowModule,
    SharedModule,
    HttpClientModule,
    AppRoutingModule,
    HomeModule,
    OurServicesModule,
    ProductModule,
    CourseModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxJsonLdModule,
    McBreadcrumbsModule.forRoot()
  ],
  declarations: [
    AppComponent,
    AboutComponent,
    ContactUsComponent
  ],
  providers: [
    // WINDOW_PROVIDERS,
    ContactUsService
    //   {provide: LOCALE_ID, useValue: 'he'}
  ],
})
export class AppModule {
  constructor(breadcrumbsConfig: McBreadcrumbsConfig,
              translate: TranslateService) {


    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('he');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use( 'he'); //window.localStorage.getItem('lang') ||


    breadcrumbsConfig.postProcess = (x) => {
      // Ensure that the first breadcrumb always points to home
      let y = x;
      if (x.length && x[0].path !== '') {
        y = [
          {
            text: 'home',
            path: ''
          }
        ].concat(x);
      }

      /** translate breadcrumbs*/
      y.forEach(p => {
        translate.get(p.text).subscribe(str => {
          p.text = str.title || p.text;
        });
      });

      return y;
    };
  }
}
