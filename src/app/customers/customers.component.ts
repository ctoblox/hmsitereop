import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.less']
})
export class CustomersComponent implements OnInit {

  customers: string[] = [
    'tosaf',
    'schindler',
    'icl',
    'osem',
    'electra',
    'simens',
    'teva',
    'haifa',
    'iec',
    'intel'
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
