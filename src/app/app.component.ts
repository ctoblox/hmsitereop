import { WINDOW } from '@ng-toolkit/universal';
import { Component, HostListener, Inject, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
// import {WINDOW} from '@app/window.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
//import 'rxjs/add/operator/filter';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/mergeMap';
import { map,  filter, mergeMap } from 'rxjs/operators';
import { HeaderComponent } from '@app/shared/components/header/header.component';
import { TranslateService } from '@ngx-translate/core';
import { ScrollToService } from 'ng2-scroll-to-el';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'app';
  dir = 'rtl';
  @ViewChild('header') header: HeaderComponent;

  schema;


  constructor(@Inject(WINDOW) private window: Window,
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private scrollService: ScrollToService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private metaService: Meta) {


  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    let offset = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    offset += 93;

    if (this.header.menuHalfKey) {
      offset *= 2;
    }
    this.header.transMenu = this.window.innerWidth > 950 && !this.header.menuVisible && offset < this.window.innerHeight;
    // console.log(offset);


  }


  ngOnInit() {
    if (this.translate.currentLang === 'en') {
      this.dir = 'ltr';
    }
    this.document.documentElement.lang = this.translate.currentLang;

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }

      /** no dedicated indexes for this entities. scroll to relevant place in home page*/
      if (evt.url === '/services' || evt.url === '/products') {
        return setTimeout(() => {
          this.scrollService.scrollTo(evt.url.replace('/', '#'), 1000, -70);
        }, 0);
      }

      window.scrollTo(0, 0);

    });

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data)
    ).subscribe((event) => {
      this.routingReady(event); 
    });

  }


  private routingReady(data) {
    this.setSEO(data);

    this.header.showMenu = false;
    this.header.menuVisible = data['menuVisible'] === true;
    this.header.menuHalfKey = data['menuHalfKey'];
    this.header.transMenu = !this.header.menuVisible;

  }

  private setSEO(data) {
    //
    let id = data.breadcrumbs;

    /** getting from server, no lang difference*/
    if (data.course) {
      this.translate.get('hm.title').subscribe((res: string) => {
        this.titleService.setTitle(data.course.title + ' | ' + res);
        this.metaService.updateTag({ name: 'description', content: data.course.shortDesc });
      });

      return;
    }

    if (data.menuHalfKey) {

      /** get meta dynamically */

      const entity = data.breadcrumbs.split('{{')[1].split('.')[0];
      id = data[entity].id;
    }

    /** get static meta*/
    this.translate.get(id + '.meta.title').subscribe((res: string) => {
      this.titleService.setTitle(res);

      const lang = this.translate.translations.he;

      this.metaService.updateTag({ name: 'description', content: lang[id].meta.desc });

      if (!this.schema) {
        this.schema = {
          '@context': 'http://schema.org',
          '@type': 'Organization',
          'logo': 'http://www.hashmal-motor.co.il/assets/images/HMLogo.png',
          'url': 'http://www.hashmal-motor.co.il',
          'name': lang.hm.title,
          'contactPoint': [
            {
              '@type': 'ContactPoint',
              'telephone': '+972-8-8554494',
              'contactType': 'customer service'
            }
          ],
          'address': {
            '@type': 'PostalAddress',
            'streetAddress': lang.hm.streetAddress,
            'addressLocality': lang.hm.addressLocality,
            'postalCode': '8487419',
            'addressCountry': 'IL'
          }
        };
      }
    });


  }


}
