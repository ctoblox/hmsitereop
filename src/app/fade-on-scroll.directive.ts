import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appFadeOnScroll]'
})
export class FadeOnScrollDirective {

  constructor(el: ElementRef) { }

  @HostListener('mouseover') onMouseOver() {
    console.log('over');
    // let part = this.el.nativeElement.querySelector('.card-text')
    // this.renderer.setElementStyle(part, 'display', 'block');
  }

  @HostListener('scroll') onScroll() {
    console.log('scrolled');
    // let part = this.el.nativeElement.querySelector('.card-text')
    // this.renderer.setElementStyle(part, 'display', 'block');
  }
}

