import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toSnake'
})
export class ToSnakePipe implements PipeTransform {

  transform(value: string): string {
    return value.split(' ').join('-');
  }

}
