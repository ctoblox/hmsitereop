import {Component, Input, OnInit} from '@angular/core';
import {Course} from '@app/course/course.model';

@Component({
  selector: 'app-course-time',
  templateUrl: './course-time.component.html',
  styleUrls: ['./course-time.component.less']
})
export class CourseTimeComponent implements OnInit {
  @Input('course') course : Course;
  constructor() { }

  ngOnInit() {
  }

}
