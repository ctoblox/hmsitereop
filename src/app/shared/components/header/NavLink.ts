export interface NavLink {
  title?: string;
  path?: string;
  id?: string;
}
