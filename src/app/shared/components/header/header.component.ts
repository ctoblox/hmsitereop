import {Component, OnInit} from '@angular/core';
import {OurServicesService} from '@app/our-services/our-services.service';

import {ProductService} from '@app/product/product.service';
import {CourseService} from '@app/course/course.service';
import {NavLink} from '@app/shared/components/header/NavLink';
import {ScrollToService} from 'ng2-scroll-to-el';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  menuVisible: boolean;
  courses: NavLink[];
  products: NavLink[];
  oursServices: NavLink[];
  showMenu = false;
  transMenu = true;
  menuHalfKey: boolean;

  constructor(private services: OurServicesService,
              private productService: ProductService,
              private scrollService: ScrollToService,
              private courseService: CourseService) {
  }

  ngOnInit() {
    this.oursServices = this.services.getLinks();

    this.products = this.productService.getLinks();

    this.courseService.getList().subscribe(res => {
      this.courses = res.splice(0, 6).map(x => {
        return {title: x.title, id: x.id};
      });
    });

  }


  scrollToElem(element) {
    setTimeout(() => {
      this.scrollService.scrollTo(element, 1000, -70); // offset -50
    }, 0);
  }

  changeLang() {
    // const cur = localStorage.getItem('lang');
    // localStorage.setItem('lang', cur === 'en' ? 'he' : 'en');
    // location.reload();
    alert('Coming soon!');
  }
}

