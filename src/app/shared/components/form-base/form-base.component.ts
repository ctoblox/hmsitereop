import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-form-base',
  templateUrl: './form-base.component.html',
  styleUrls: ['./form-base.component.less']
})
export class FormBaseComponent {
  form: FormGroup;
  successMsg: string = null;
  failed: string = null;
  disableBtn = false;

  constructor(private location: Location) {
  }


  submitForm() {
    if (!this.form.valid) {
      return this.validateAllFormFields(this.form);
    }

    this.disableBtn = true;
    this.successMsg = this.failed = null;
    this.postMe(this.form.value);
  }

  postMe(data) {

  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-value': this.hasValue(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  goBack() {
    this.location.back();
  }

  private hasValue(field: string) {
    return this.form.get(field).value;
  }
}
