import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-field-error-display',
  templateUrl: './field-error-display.component.html',
  styleUrls: ['./field-error-display.component.less']
})
export class FieldErrorDisplayComponent {
  @Input('errorMsg') errorMsg: string;
  @Input('displayError') displayError: boolean;
}
