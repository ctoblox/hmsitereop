import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from '@app/shared/components/header/header.component';
import {FooterComponent} from '@app/shared/components/footer/footer.component';
import {ToSnakePipe} from './pipes/to-snake/to-snake.pipe';
import {AppRoutingModule} from '@app/app-routing.module';

import { AgmCoreModule } from '@agm/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FieldErrorDisplayComponent } from './components/field-error-display/field-error-display.component';
import { FormBaseComponent } from './components/form-base/form-base.component';
import {ScrollToModule} from 'ng2-scroll-to-el';
import { TranslateModule, TranslateService} from '@ngx-translate/core';
import { CourseTimeComponent } from './components/course-time/course-time.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ScrollToModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA9q-hqlPJQTFh1MyakNhJaDLqF3JVTm2U'
    }),
    TranslateModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    ToSnakePipe,
    FieldErrorDisplayComponent,
    FormBaseComponent,
    CourseTimeComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    FieldErrorDisplayComponent,
    CourseTimeComponent,
    FormBaseComponent,
    ToSnakePipe,
    FormsModule,
    TranslateModule,
    ScrollToModule,
    ReactiveFormsModule
  ]
})
export class SharedModule {

}
