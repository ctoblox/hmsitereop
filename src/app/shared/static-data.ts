
export class StaticDataService {
  buildLink(entity: string, list: any) {
    list.forEach(x => {
      x.link = entity + '/' + x.title.split(' ').join('-');
    });

    return list;
  }
}
