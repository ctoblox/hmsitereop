export interface Product {
  id: string;
  path: string;
  title?: string;
  img?: string;
  logo?: string;
  link?: string;
  desc?: string;
  titleClass?: string;
  overlayClass?: string;
  text?: string;
  piclessTile?: boolean;
}
