import { Component, OnInit } from '@angular/core';
import { ProductService} from '../product.service';
import {Product} from '@app/product/product.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less']
})
export class ProductListComponent implements OnInit {
  products: Product[];

  constructor(private service: ProductService) {
  }

  ngOnInit() {
    this.products = this.service.get();
  }
}
