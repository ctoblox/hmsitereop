import {Injectable} from '@angular/core';

import {StaticDataService} from '@app/shared/static-data';
import {NavLink} from '@app/shared/components/header/NavLink';
import {Product} from '@app/product/product.model';

const PRODUCTS: Product[] = [
  {
    id: 'rings',
    path: 'grounding-rings', //
    logo: 'AEGIS.png'
  },
  {
    id: 'hydr',
    path: 'hydraulic-pullers',
    logo: 'omar-star.png',
  },
  {
    id: 'shims',
    path: 'shims',
    logo: ''
  },
  {
    id: 'lazereq',
    path: 'laser-alignment-instruments',
    logo: 'easy-laser.png',
  },
  {
    id: 'elctv',
    path: 'electronic-testing-systems',
    logo: 'schleich.png'
  },
  {
    id: 'vbMonitor',
    path: 'vb-sensors-and-cables',
    logo: 'hansford-sensors.png'
  }
];

@Injectable()
export class ProductService extends StaticDataService {

  constructor() {
    super();

  }

  get(): Product[] {
    return PRODUCTS;
  }

  getSingle(path: string): Product {
    // const id = title.split('-').join(' ');
    return this.get().find(item => item.path === path) as Product;

  }

  getLinks(): NavLink[] {
    return this.get().map(x => {
      return {id: x.id, path: x.path} as NavLink;
    });
  }
}


