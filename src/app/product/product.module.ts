import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductDetailsComponent} from './product-details/product-details.component';
import { ProductService} from './product.service';
import {ProductListComponent} from './product-list/product-list.component';
import {McBreadcrumbsModule} from 'ngx-breadcrumbs';
import {AppRoutingModule} from '@app/app-routing.module';
import {SharedModule} from '@app/shared/shared.module';
import {ProductResolve} from '@app/product/product.resolve';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    McBreadcrumbsModule.forRoot()
  ],
  declarations: [ProductDetailsComponent, ProductListComponent],
  providers: [ProductService, ProductResolve]
})
export class ProductModule {
}
