import {Injectable} from '@angular/core';

import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ProductService} from '@app/product/product.service';


@Injectable()
export class ProductResolve implements Resolve<any> {

  constructor(private productsService: ProductService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.productsService.getSingle(route.params['id']);
  }
}
