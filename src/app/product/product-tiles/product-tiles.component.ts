import {Component, OnInit} from '@angular/core';
import {ProductService} from '../product.service';
import {Product} from '@app/product/product.model';

@Component({
  selector: 'app-product-tiles',
  templateUrl: './product-tiles.component.html',
  styleUrls: ['./product-tiles.component.less']
})
export class ProductTilesComponent implements OnInit {
  products: Product[];
  visId: number;
  cnt: number;
  SWIPE_ACTION = {LEFT: 'swipeleft', RIGHT: 'swiperight'};
  private timer: number;

  constructor(private service: ProductService) {

  }


  ngOnInit() {
    this.products = this.service.get();
    this.cnt = this.products.length;
    this.startLoop(0);
  }

  nextProduct(index: number) {
    clearTimeout(this.timer);
    this.startLoop(index, 2);
  }


  startLoop(slide: number, loop: number = 1) {
    this.visId = slide;
    this.timer = window.setTimeout(() => {
      this.startLoop(slide + 1 === this.cnt ? 0 : slide + 1);
    }, 5 * loop * 1000);
  }

  swipe(currentIndex: number, action = this.SWIPE_ACTION.RIGHT) {
    // out of range
    if (currentIndex > this.products.length || currentIndex < 0) {
      return;
    }

    let nextIndex = 0;

    // swipe right, next avatar
    if (action === this.SWIPE_ACTION.RIGHT) {
      const isLast = currentIndex === this.products.length - 1;
      nextIndex = isLast ? 0 : currentIndex + 1;
    }

    // swipe left, previous avatar
    if (action === this.SWIPE_ACTION.LEFT) {
      const isFirst = currentIndex === 0;
      nextIndex = isFirst ? this.products.length - 1 : currentIndex - 1;
    }

    // toggle avatar visibilityy

    this.nextProduct(nextIndex);
  }
}
